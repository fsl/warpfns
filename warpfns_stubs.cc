////////////////////////////////////////////////////////////////
// This file contains template instanatiations of functions from
// warpfns.h for floating point. These are solely here to catch
// syntax errors in warpfns.h when building warpfns.
////////////////////////////////////////////////////////////////

#include <vector>

#include "warpfns.h"

#include "armawrap/newmat.h"
#include "NewNifti/NewNifti.h"
#include "utils/threading.h"
#include "newimage/newimageall.h"

namespace NEWIMAGE {

  template NEWMAT::ReturnMatrix NewimageCoord2NewimageCoord(
    const NEWMAT::Matrix&        M,
    const volume<float>&         srcvol,
    const volume<float>&         destvol,
    const NEWMAT::ColumnVector&  srccoord);

  template NEWMAT::ReturnMatrix NewimageCoord2NewimageCoord(
    const volume4D<float>&       warps,
    bool                         inv_flag,
    const volume<float>&         srcvol,
    const volume<float>&         destvol,
    const NEWMAT::ColumnVector&  srccoord);
  template NEWMAT::ReturnMatrix NewimageCoord2NewimageCoord(
    const volume4D<float>&       warps,
    bool                         inv_flag,
    const NEWMAT::Matrix&        M,
    const volume<float>&         srcvol,
    const volume<float>&         destvol,
    const NEWMAT::ColumnVector&  srccoord);
  template NEWMAT::ReturnMatrix NewimageCoord2NewimageCoord(
    const NEWMAT::Matrix&        M,
    const volume4D<float>&       warps,
    bool                         inv_flag,
    const volume<float>&         srcvol,
    const volume<float>&         destvol,
    const NEWMAT::ColumnVector&  srccoord);
  template NEWMAT::ReturnMatrix NewimageCoord2NewimageCoord(
    const NEWMAT::Matrix&        M1,
    const volume4D<float>&       warps,
    bool                         inv_flag,
    const NEWMAT::Matrix&        M2,
    const volume<float>&         srcvol,
    const volume<float>&         destvol,
    const NEWMAT::ColumnVector&  srccoord);

  template void raw_general_transform(
    const volume<float>&     vin,
    const NEWMAT::Matrix&    A,
    const volume4D<float>&   d,
    const std::vector<int>&  defdir,
    const std::vector<int>&  derivdir,
    volume<float>&           vout,
    volume4D<float>&         deriv,
    Utilities::NoOfThreads   nthreads);
  template void raw_general_transform(
    const volume<float>&     vin,
    const NEWMAT::Matrix&    A,
    const volume4D<float>&   d,
    const std::vector<int>&  defdir,
    const std::vector<int>&  derivdir,
    volume<float>&           vout,
    volume4D<float>&         deriv,
    volume<char>&            invol,
    Utilities::NoOfThreads   nthreads);

  template void apply_warp(
    const volume<float>&   vin,
    const NEWMAT::Matrix&  A,
    const volume4D<float>  d,
    const NEWMAT::Matrix&  TT,
    const NEWMAT::Matrix&  M,
    volume<float>&         vout,
    Utilities::NoOfThreads nthreads);
  template void apply_warp(
    const volume<float>&   vin,
    const NEWMAT::Matrix&  A,
    const volume4D<float>  d,
    const NEWMAT::Matrix&  TT,
    const NEWMAT::Matrix&  M,
    volume<float>&         vout,
    volume<char>&          mask,
    Utilities::NoOfThreads nthreads);
  template void apply_warp(
    const volume<float>&             vin,
    const NEWMAT::Matrix&            A,
    const volume4D<float>            d,
    const NEWMAT::Matrix&            TT,
    const NEWMAT::Matrix&            M,
    const std::vector<unsigned int>& slices,
    volume<float>&                   vout,
    volume<char>&                    mask,
    Utilities::NoOfThreads           nthreads);
  template int apply_warp(
    const volume<float>&   invol,
    volume<float>&         outvol,
    const volume4D<float>& warpvol,
    Utilities::NoOfThreads nthreads);

  template void affine_transform(
    const volume<float>&   vin,
    const NEWMAT::Matrix&  aff,
    volume<float>&         vout,
    Utilities::NoOfThreads nthreads);
  template void affine_transform(
    const volume<float>&   vin,
    const NEWMAT::Matrix&  aff,
    volume<float>&         vout,
    volume<char>&          inside_volume,
    Utilities::NoOfThreads nthreads);
  template void affine_transform(
    const volume<float>&             vin,
    const NEWMAT::Matrix&            aff,
    const std::vector<unsigned int>& slices,
    volume<float>&                   vout,
    volume<char>&                    inside_volume,
    Utilities::NoOfThreads           nthreads);
  template void affine_transform_3partial(
    const volume<float>&   vin,
    const NEWMAT::Matrix&  aff,
    volume<float>&         vout,
    volume4D<float>&       deriv,
    Utilities::NoOfThreads nthreads);
  template void affine_transform_3partial(
    const volume<float>&   vin,
    const NEWMAT::Matrix&  aff,
    volume<float>&         vout,
    volume4D<float>&       deriv,
    volume<char>&          invol,
    Utilities::NoOfThreads nthreads);

  template void displacement_transform_1D(
    const volume<float>&   vin,
    const NEWMAT::Matrix&  aff,
    const volume<float>&   df,
    int                    defdir,
    volume<float>&         vout,
    Utilities::NoOfThreads nthreads);
  template void displacement_transform_1D(
    const volume<float>&   vin,
    const NEWMAT::Matrix&  aff,
    const volume<float>&   df,
    int                    defdir,
    volume<float>&         vout,
    volume<char>&          invol,
    Utilities::NoOfThreads nthreads);
  template void displacement_transform_1D_3partial(
    const volume<float>&   vin,
    const NEWMAT::Matrix&  aff,
    const volume<float>&   df,
    int                    dir,
    volume<float>&         vout,
    volume4D<float>&       deriv,
    Utilities::NoOfThreads nthreads);
  template void displacement_transform_1D_3partial(
    const volume<float>&   vin,
    const NEWMAT::Matrix&  aff,
    const volume<float>&   df,
    int                    dir,
    volume<float>&         vout,
    volume4D<float>&       deriv,
    volume<char>&          invol,
    Utilities::NoOfThreads nthreads);

  template void general_transform(
    const volume<float>&     vin,
    const NEWMAT::Matrix&    aff,
    const volume4D<float>&   df,
    volume<float>&           vout,
    Utilities::NoOfThreads   nthreads);
  template void general_transform(
    const volume<float>&     vin,
    const NEWMAT::Matrix&    aff,
    const volume4D<float>&   df,
    volume<float>&           vout,
    volume<char>&            invol,
    Utilities::NoOfThreads   nthreads);
  template void general_transform_3partial(
    const volume<float>&     vin,
    const NEWMAT::Matrix&    aff,
    const volume4D<float>&   df,
    volume<float>&           vout,
    volume4D<float>&         deriv,
    Utilities::NoOfThreads   nthreads);
  template void general_transform_3partial(
    const volume<float>&         vin,
    const NEWMAT::Matrix&    aff,
    const volume4D<float>&   df,
    volume<float>&           vout,
    volume4D<float>&         deriv,
    volume<char>&            invol,
    Utilities::NoOfThreads   nthreads);

  template void get_displacement_fields(
    const volume<float>&     vin,
    const NEWMAT::Matrix&    aff,
    const volume4D<float>&   dfin,
    volume4D<float>&         dfout);

} // namespace NEWIMAGE


namespace RGT_UTILS { // raw_general_transform utilities

  template std::tuple<bool,std::string> validate_input(
    const NEWMAT::Matrix&             A,
    const NEWMAT::Matrix              *TT,
    const NEWMAT::Matrix              *M,
    const NEWIMAGE::volume4D<float>&  d,
    const std::vector<int>&           defdir,
    const std::vector<int>&           derivdir,
    const std::vector<unsigned int>&  slices,
    const NEWIMAGE::volume<float>&    out,
    const NEWIMAGE::volume4D<float>&  deriv,
    const NEWIMAGE::volume<char>      *valid);

  template std::pair<NEWIMAGE::volume<float>::extrapparams,
                     NEWIMAGE::volume<float>::extrapparams>
  calculate_extrapolation(
    const NEWIMAGE::volume<float>&               f,
    const NEWIMAGE::volume4D<float>&             d,
    const NEWIMAGE::volume<float>::extrapparams& infext,
    const NEWIMAGE::volume<float>::extrapparams& indext);

  template std::tuple<NEWMAT::Matrix,bool> make_iT(
    const NEWIMAGE::volume4D<float>& d,
    const NEWIMAGE::volume<float>&   out,
    const NEWMAT::Matrix             *TT);

  template void affine_no_derivs(
    unsigned int                                 first_j,
    unsigned int                                 last_j,
    const NEWIMAGE::volume<float>&               f,
    const std::vector<unsigned int>&             slices,
    const NEWMAT::Matrix&                        A,
    const NEWIMAGE::volume<float>::extrapparams& fext,
    NEWIMAGE::volume<float>&                     out,
    NEWIMAGE::volume<char>                      *valid);

  template void affine_with_derivs(
    unsigned int                                 first_j,
    unsigned int                                 last_j,
    const NEWIMAGE::volume<float>&               f,
    const std::vector<unsigned int>&             slices,
    const NEWMAT::Matrix&                        A,
    const std::vector<int>&                      derivdir,
    const NEWIMAGE::volume<float>::extrapparams& fext,
    NEWIMAGE::volume<float>&                     out,
    NEWIMAGE::volume4D<float>&                   deriv,
    NEWIMAGE::volume<char>                      *valid);

  template void displacements_no_iT(
    unsigned int                                 first_j,
    unsigned int                                 last_j,
    const NEWIMAGE::volume<float>&               f,
    const std::vector<unsigned int>&             slices,
    const NEWMAT::Matrix&                        A,
    const NEWIMAGE::volume4D<float>&             d,
    const std::vector<int>&                      defdir,
    const NEWMAT::Matrix&                        M,
    const std::vector<int>&                      derivdir,
    const NEWIMAGE::volume<float>::extrapparams& fext,
    const NEWIMAGE::volume<float>::extrapparams& dext,
    NEWIMAGE::volume<float>&                     out,
    NEWIMAGE::volume<float>&                     deriv,
    NEWIMAGE::volume<char>                      *valid);

  template void displacements_with_iT(
    unsigned int                                 first_j,
    unsigned int                                 last_j,
    const NEWIMAGE::volume<float>&               f,
    const std::vector<unsigned int>&             slices,
    const NEWMAT::Matrix&                        iT,
    const NEWMAT::Matrix&                        A,
    const NEWIMAGE::volume4D<float>&             d,
    const std::vector<int>&                      defdir,
    const NEWMAT::Matrix&                        M,
    const std::vector<int>&                      derivdir,
    const NEWIMAGE::volume<float>::extrapparams& fext,
    const NEWIMAGE::volume<float>::extrapparams& dext,
    NEWIMAGE::volume<float>&                     out,
    NEWIMAGE::volume<float>&                     deriv,
    NEWIMAGE::volume<char>                      *valid);

  template void set_sqform(
    const NEWIMAGE::volume<float>&    f,
    const NEWIMAGE::volume4D<float>&  d,
    const std::vector<int>&           defdir,
    NEWMAT::Matrix                    iA,
    const NEWMAT::Matrix              *TT,
    const NEWMAT::Matrix              *M,
    NEWIMAGE::volume<float>&          out,
    NEWIMAGE::volume4D<float>&        deriv);

} // End namespace RGT_UTILS
