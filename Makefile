# A Makefile for warpfns library

include ${FSLCONFDIR}/default.mk

PROJNAME = warpfns
LIBS     = -lfsl-basisfield -lfsl-meshclass -lfsl-newimage \
           -lfsl-miscmaths -lfsl-NewNifti -lfsl-utils
OBJS     = warpfns.o warpfns_stubs.o fnirt_file_reader.o \
           fnirt_file_writer.o point_list.o

all: libfsl-warpfns.so

libfsl-warpfns.so: ${OBJS}
	$(CXX) $(CXXFLAGS) -shared -o $@ $^ ${LDFLAGS}
